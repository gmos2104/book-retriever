# Book Retriever

An API that fetches books and saves them in a local db

The application will create two endpoints:

- GET / To retrieve saved books
- POST / To post a book retrieve action with the following format:

```
{
  "event": string,
  "action": "GET" | "LIST",
  "code": string?
}
```

Some examples:

```
GET http://localhost:3000/
```

```
POST http://localhost:3000/
{
   "event": "bookrequest",
   "action": "GET",
   "code": "65a9a8ab22d2eb408ca77c8c"
}
```

```
POST http://localhost:3000/
{
   "event": "bookrequest",
   "action": "LIST"
}
```

## Running the project

### Development

- Enable corepack: `corepack enable`

- Install the dependencies: `pnpm install`

- Run the project: `pnpm run dev`

- To execute the tests: `pnpm run test`

### Production build

- Install the dependencies: `pnpm install`

- Compile TS files: `pnpm run build`

- Remove dev dependencies: `pnpm prune --production`

- Run the project: `pnpm run start`

### Docker

To run the application with docker just run: `docker compose up -d`
