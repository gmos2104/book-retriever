import type { JestConfigWithTsJest } from "ts-jest";

const config: JestConfigWithTsJest = {
  //collectCoverage: true,
  //coverageThreshold: {
  //  global: {
  //    branches: 80,
  //    functions: 80,
  //    lines: 80,
  //    statements: 80,
  //  },
  //},
  verbose: true,
  transform: {
    "^.+\\.tsx?$": "ts-jest",
  },
  setupFiles: ["<rootDir>/tests/setup.ts"],
};

export default config;
