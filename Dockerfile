FROM node:20 AS builder

WORKDIR /usr/src/app

COPY ./src ./src
COPY ./package.json ./package.json
COPY ./pnpm-lock.yaml ./pnpm-lock.yaml
COPY ./tsconfig.json ./tsconfig.json

RUN corepack enable
RUN pnpm install
RUN pnpm run build
RUN pnpm prune --production

FROM node:20

WORKDIR /usr/src/app

COPY --from=builder /usr/src/app/dist ./dist
COPY --from=builder /usr/src/app/node_modules ./node_modules
COPY --from=builder /usr/src/app/package.json ./package.json

EXPOSE 3000

CMD ["node", "dist/main.js"]
