import { RetrieveSavedBooksUseCase } from "../../../../../src/modules/book/application/use-case/retrieve-saved-books.use-case";
import { ReadableBookRepository } from "../../../../../src/modules/book/domain/interfaces/book.repository";

describe("RetrieveSavedBooksUseCase", () => {
  const repository: ReadableBookRepository = {
    all: jest.fn(),
    find: jest.fn(),
  };

  afterEach(() => {
    jest.clearAllMocks();
  });

  test("call repository", async () => {
    const useCase = new RetrieveSavedBooksUseCase(repository);

    await useCase.execute();

    expect(repository.all).toHaveBeenCalled();
  });
});
