import { faker } from "@faker-js/faker";
import { ProcessBookEventUseCase } from "../../../../../src/modules/book/application/use-case/process-book-event.use-case";
import { EventAction } from "../../../../../src/modules/book/domain/enums/event-action.enum";
import { BookRetrieveEvent } from "../../../../../src/modules/book/infrastructure/dtos/event.dto";
import { Book } from "../../../../../src/modules/book/domain/entities/book.entity";

describe("ProcessBookEventUseCase", () => {
  const readableRepository = {
    all: jest.fn(),
    find: jest.fn(),
  };
  const writeableRepository = {
    save: jest.fn(),
    saveMany: jest.fn(),
  };

  afterEach(() => {
    jest.clearAllMocks();
  });

  test("save single book", async () => {
    const useCase = new ProcessBookEventUseCase(
      readableRepository,
      writeableRepository,
    );

    const event = new BookRetrieveEvent(
      "bookrequest",
      EventAction.GET,
      faker.string.uuid(),
    );

    await useCase.execute(event);

    expect(readableRepository.find).toHaveBeenCalledWith(event.code);
    expect(readableRepository.all).not.toHaveBeenCalled();
    expect(writeableRepository.save).toHaveBeenCalled();
    expect(writeableRepository.saveMany).not.toHaveBeenCalled();
  });

  test("save multiple books", async () => {
    const useCase = new ProcessBookEventUseCase(
      readableRepository,
      writeableRepository,
    );

    const event = new BookRetrieveEvent("bookrequest", EventAction.LIST);

    const books = [
      new Book(
        faker.string.uuid(),
        faker.lorem.words(),
        faker.person.fullName(),
        faker.date.past().getFullYear(),
      ),
    ];

    readableRepository.all.mockResolvedValueOnce(books);

    await useCase.execute(event);

    expect(readableRepository.find).not.toHaveBeenCalled();
    expect(readableRepository.all).toHaveBeenCalledWith();
    expect(writeableRepository.save).not.toHaveBeenCalled();
    expect(writeableRepository.saveMany).toHaveBeenCalledWith(books);
  });
});
