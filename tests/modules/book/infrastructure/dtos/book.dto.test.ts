import { faker } from "@faker-js/faker";

import { BookDTO } from "../../../../../src/modules/book/infrastructure/dtos/book.dto";

describe("BookDTO", () => {
  test("parse to domain", () => {
    const date = faker.date.past();
    const dto = new BookDTO(
      faker.string.uuid(),
      faker.lorem.words(),
      date.toISOString(),
      faker.person.fullName(),
    );

    expect(BookDTO.toDomain(dto)).toEqual({
      id: dto._id,
      title: dto.name,
      author: dto.author,
      year: date.getFullYear(),
    });
  });
});
