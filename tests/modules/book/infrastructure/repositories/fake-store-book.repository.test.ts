import { faker } from "@faker-js/faker";
import { FakeStoreBookRepository } from "../../../../../src/modules/book/infrastructure/repositories/fake-store-book.repository";
import { BookDTO } from "../../../../../src/modules/book/infrastructure/dtos/book.dto";
import { DataNotFoundError } from "../../../../../src/modules/book/infrastructure/errors/data-not-found.error";
import { ServiceUnavailableError } from "../../../../../src/modules/book/infrastructure/errors/service-unavailable.error";

describe("FakeStoreBookRepository", () => {
  const fetchMock = jest.spyOn(global, "fetch");

  afterAll(() => {
    jest.clearAllMocks();
  });

  test("fetches a single book", async () => {
    const repository = new FakeStoreBookRepository();

    const book = new BookDTO(
      faker.string.uuid(),
      faker.lorem.words(),
      faker.date.past().toISOString(),
      faker.person.fullName(),
    );

    fetchMock.mockResolvedValueOnce({
      ok: true,
      json: () => Promise.resolve(book),
    } as Response);

    const result = await repository.find(book._id);
    expect(fetchMock).toHaveBeenCalled();
    expect(result).toStrictEqual(BookDTO.toDomain(book));
  });

  test("fetches multiple books", async () => {
    const repository = new FakeStoreBookRepository();

    const books = [
      new BookDTO(
        faker.string.uuid(),
        faker.lorem.words(),
        faker.date.past().toISOString(),
        faker.person.fullName(),
      ),
      new BookDTO(
        faker.string.uuid(),
        faker.lorem.words(),
        faker.date.past().toISOString(),
        faker.person.fullName(),
      ),
    ];

    fetchMock.mockResolvedValueOnce({
      ok: true,
      json: () => Promise.resolve(books),
    } as Response);

    const result = await repository.all();
    expect(fetchMock).toHaveBeenCalled();
    expect(result).toStrictEqual(books.map(BookDTO.toDomain));
  });

  test("throw DataNotFoundError when all returns status 400", async () => {
    const repository = new FakeStoreBookRepository();

    fetchMock.mockResolvedValueOnce({
      ok: false,
      status: 400,
    } as Response);

    try {
      await repository.all();
    } catch (error) {
      expect(error instanceof DataNotFoundError).toBe(true);
    }
  });

  test("throw DataNotFoundError when find returns status 400", async () => {
    const repository = new FakeStoreBookRepository();

    fetchMock.mockResolvedValueOnce({
      ok: false,
      status: 400,
    } as Response);

    try {
      await repository.find(faker.string.uuid());
    } catch (error) {
      expect(error instanceof DataNotFoundError).toBe(true);
    }
  });

  test("throw ServiceUnavailableError when all returns status 500", async () => {
    const repository = new FakeStoreBookRepository();

    fetchMock.mockResolvedValueOnce({
      ok: false,
      status: 500,
    } as Response);

    try {
      await repository.all();
    } catch (error) {
      expect(error instanceof ServiceUnavailableError).toBe(true);
    }
  });

  test("throw ServiceUnavailableError when find returns status 500", async () => {
    const repository = new FakeStoreBookRepository();

    fetchMock.mockResolvedValueOnce({
      ok: false,
      status: 500,
    } as Response);

    try {
      await repository.find(faker.string.uuid());
    } catch (error) {
      expect(error instanceof ServiceUnavailableError).toBe(true);
    }
  });
});
