import express from "express";
import { faker } from "@faker-js/faker";
import { EventAction } from "../../../../../src/modules/book/domain/enums/event-action.enum";
import { handler } from "../../../../../src/modules/book/infrastructure/controllers/process-book-event.controller";
import { BookRetrieveEvent } from "../../../../../src/modules/book/infrastructure/dtos/event.dto";
import { ProcessBookEventUseCase } from "../../../../../src/modules/book/application/use-case/process-book-event.use-case";
import { DataNotFoundError } from "../../../../../src/modules/book/infrastructure/errors/data-not-found.error";
import { ServiceUnavailableError } from "../../../../../src/modules/book/infrastructure/errors/service-unavailable.error";

describe("processBookEventHandler", () => {
  const useCaseMocked = jest.spyOn(
    ProcessBookEventUseCase.prototype,
    "execute",
  );

  afterEach(() => {
    jest.resetAllMocks();
  });

  afterAll(() => {
    jest.clearAllMocks();
  });

  test("should return 201 with correct data", async () => {
    useCaseMocked.mockResolvedValueOnce();

    const request = {
      body: new BookRetrieveEvent(
        "bookrequest",
        EventAction.GET,
        faker.string.uuid(),
      ),
    } as express.Request;

    const response = {
      status: jest.fn().mockReturnValue({
        send: jest.fn(),
        json: jest.fn(),
      }),
    } as unknown as express.Response;

    await handler(request, response);

    expect(useCaseMocked).toHaveBeenCalled();
    expect(response.status).toHaveBeenCalledWith(201);
  });

  test("should return 400 with incorrect data", async () => {
    useCaseMocked.mockResolvedValueOnce();

    const request = {
      // GET action without code
      body: new BookRetrieveEvent("bookrequest", EventAction.GET),
    } as express.Request;

    const response = {
      status: jest.fn().mockReturnValue({
        send: jest.fn(),
        json: jest.fn(),
      }),
    } as unknown as express.Response;

    await handler(request, response);

    expect(useCaseMocked).not.toHaveBeenCalled();
    expect(response.status).toHaveBeenCalledWith(400);
  });

  test("should return 404 when no data is found", async () => {
    useCaseMocked.mockImplementationOnce(() => {
      throw new DataNotFoundError(EventAction.LIST);
    });

    const request = {
      body: new BookRetrieveEvent("bookrequest", EventAction.LIST),
    } as express.Request;

    const response = {
      status: jest.fn().mockReturnValue({
        send: jest.fn(),
        json: jest.fn(),
      }),
    } as unknown as express.Response;

    await handler(request, response);

    expect(useCaseMocked).toHaveBeenCalled();
    expect(response.status).toHaveBeenCalledWith(404);
  });

  test("should return 500 when a service error occurs", async () => {
    useCaseMocked.mockImplementationOnce(() => {
      throw new ServiceUnavailableError();
    });

    const request = {
      body: new BookRetrieveEvent("bookrequest", EventAction.LIST),
    } as express.Request;

    const response = {
      status: jest.fn().mockReturnValue({
        send: jest.fn(),
        json: jest.fn(),
      }),
    } as unknown as express.Response;

    await handler(request, response);

    expect(useCaseMocked).toHaveBeenCalled();
    expect(response.status).toHaveBeenCalledWith(500);
  });
});
