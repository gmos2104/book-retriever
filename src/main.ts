import express from "express";
import { handler as getSavedBooksHandler } from "./modules/book/infrastructure/controllers/get-saved-books.controller";
import { handler as processBookEventHandler } from "./modules/book/infrastructure/controllers/process-book-event.controller";

const port = process.env.PORT || 3000;
const app = express();

app.use(express.json());

app.get("/", getSavedBooksHandler);
app.post("/", processBookEventHandler);

app.listen(Number(port), () => {
  console.info(`Started server at port ${port}`);
});
