import { Book } from "../entities/book.entity";

export interface ReadableBookRepository {
  all(): Promise<Book[]>;
  find(code: string): Promise<Book>;
}

export interface WriteableBookRepository {
  save(book: Book): Promise<void>;
  saveMany(books: Book[]): Promise<void>;
}
