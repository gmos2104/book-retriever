import { BookRetrieveEvent } from "../../infrastructure/dtos/event.dto";

export interface FetchAndSaveStrategy {
  execute(event: BookRetrieveEvent): Promise<void>;
}
