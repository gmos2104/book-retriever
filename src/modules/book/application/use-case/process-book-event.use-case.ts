import { BookRetrieveEvent } from "../../infrastructure/dtos/event.dto";
import {
  ReadableBookRepository,
  WriteableBookRepository,
} from "../../domain/interfaces/book.repository";
import { FetchAndSaveSingleBookStrategy } from "../strategies/fetch-and-save-single-book.strategy";
import { FetchAndSaveMultipleBooksStrategy } from "../strategies/fetch-and-save-multiple-books.strategy";
import { FetchAndSaveStrategy } from "../interfaces/fetch-and-save.strategy";
import { EventAction } from "../../domain/enums/event-action.enum";

export class ProcessBookEventUseCase {
  constructor(
    private readonly readBookRepository: ReadableBookRepository,
    private readonly writeBookRepository: WriteableBookRepository,
  ) {}

  async execute(event: BookRetrieveEvent): Promise<void> {
    let strategy: FetchAndSaveStrategy;

    switch (event.action) {
      case EventAction.GET:
        strategy = new FetchAndSaveSingleBookStrategy(
          this.readBookRepository,
          this.writeBookRepository,
        );
        break;
      case EventAction.LIST:
        strategy = new FetchAndSaveMultipleBooksStrategy(
          this.readBookRepository,
          this.writeBookRepository,
        );
        break;
    }

    await strategy.execute(event);
  }
}
