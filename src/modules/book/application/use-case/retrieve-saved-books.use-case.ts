import { Book } from "../../domain/entities/book.entity";
import { ReadableBookRepository } from "../../domain/interfaces/book.repository";

export class RetrieveSavedBooksUseCase {
  constructor(private readonly readBookRepository: ReadableBookRepository) {}

  execute(): Promise<Book[]> {
    return this.readBookRepository.all();
  }
}
