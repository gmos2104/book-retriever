import {
  ReadableBookRepository,
  WriteableBookRepository,
} from "../../domain/interfaces/book.repository";
import { BookRetrieveEvent } from "../../infrastructure/dtos/event.dto";
import { FetchAndSaveStrategy } from "../interfaces/fetch-and-save.strategy";

export class FetchAndSaveSingleBookStrategy implements FetchAndSaveStrategy {
  constructor(
    private readonly readBookRepository: ReadableBookRepository,
    private readonly writeBookRepository: WriteableBookRepository,
  ) {}

  async execute(event: BookRetrieveEvent): Promise<void> {
    if (!event.code) {
      throw new Error("Code required to lookup book");
    }

    const result = await this.readBookRepository.find(event.code);

    await this.writeBookRepository.save(result);
  }
}
