import {
  ReadableBookRepository,
  WriteableBookRepository,
} from "../../domain/interfaces/book.repository";
import { BookRetrieveEvent } from "../../infrastructure/dtos/event.dto";
import { FetchAndSaveStrategy } from "../interfaces/fetch-and-save.strategy";

export class FetchAndSaveMultipleBooksStrategy implements FetchAndSaveStrategy {
  constructor(
    private readonly readBookRepository: ReadableBookRepository,
    private readonly writeBookRepository: WriteableBookRepository,
  ) {}

  async execute(_: BookRetrieveEvent): Promise<void> {
    const result = await this.readBookRepository.all();

    await this.writeBookRepository.saveMany(result);
  }
}
