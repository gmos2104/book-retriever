import { z } from "zod";
import { EventAction } from "../../domain/enums/event-action.enum";

export const eventSchema = z
  .object({
    event: z.string(),
    action: z.nativeEnum(EventAction, {
      message: "Invalid action. Must be GET or LIST",
    }),
    code: z.string().optional(),
  })
  .refine(
    (input) => {
      if (input.action === EventAction.GET && !input.code) return false;

      return true;
    },
    {
      message: "Code is required when using GET action",
    },
  );
