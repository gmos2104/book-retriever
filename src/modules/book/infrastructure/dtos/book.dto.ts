import { Book } from "../../domain/entities/book.entity";

export class BookDTO {
  constructor(
    public _id: string,
    public name: string,
    public publication_date: string,
    public author: string,
  ) {}

  public static toDomain(dto: BookDTO): Book {
    return new Book(
      dto._id,
      dto.name,
      dto.author,
      new Date(dto.publication_date).getFullYear(),
    );
  }
}
