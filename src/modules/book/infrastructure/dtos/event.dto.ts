import { EventAction } from "../../domain/enums/event-action.enum";

export class BookRetrieveEvent {
  constructor(
    public event: string,
    public action: EventAction,
    public code?: string,
  ) {}
}
