import express from "express";
import { processBookEventUseCase } from "../ioc";
import { eventSchema } from "../schemas/event.schema";
import { ZodError } from "zod";
import { DataNotFoundError } from "../errors/data-not-found.error";

export const handler = async (req: express.Request, res: express.Response) => {
  try {
    const event = eventSchema.parse(req.body);

    await processBookEventUseCase.execute(event);
    res.status(201).send();
  } catch (error) {
    if (error instanceof ZodError) {
      res.status(400).json({
        errors: error.issues.map((issue) => issue.message),
      });
    } else if (error instanceof DataNotFoundError) {
      res.status(404).json({
        message: error.message,
      });
    } else {
      res.status(500).json({
        message: "An error ocurred",
      });
    }
  }
};
