import express from "express";
import { retrieveSavedBooksUseCase } from "../ioc";

export const handler = async (_: express.Request, res: express.Response) => {
  const books = await retrieveSavedBooksUseCase.execute();

  res.status(200).json(books);
};
