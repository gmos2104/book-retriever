export class ServiceUnavailableError extends Error {
  message = "Service is currently unavailable";
}
