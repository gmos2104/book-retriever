import { EventAction } from "../../domain/enums/event-action.enum";

export class DataNotFoundError extends Error {
  constructor(action: EventAction, code?: string) {
    super();

    if (action === EventAction.GET) {
      this.message = `No data found for action ${action} with code ${code}`;
    } else {
      this.message = `No data found for action ${action}`;
    }
  }
}
