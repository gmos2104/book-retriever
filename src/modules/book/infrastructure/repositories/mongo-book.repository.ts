import { MongoClient } from "mongodb";
import { Book } from "../../domain/entities/book.entity";
import {
  ReadableBookRepository,
  WriteableBookRepository,
} from "../../domain/interfaces/book.repository";

export class MongoBookRepository
  implements ReadableBookRepository, WriteableBookRepository
{
  private client: MongoClient;

  constructor() {
    const url = process.env.DB_URL;

    if (!url) {
      throw new Error("Improperly configured");
    }

    this.client = new MongoClient(url);
  }

  async all(): Promise<Book[]> {
    const database = this.client.db("books");
    const cursor = database.collection<Book>("saved").find();

    const books = [];

    for await (const book of cursor) {
      books.push(book);
    }

    return books;
  }

  async find(code: string): Promise<Book> {
    const book = await this.client
      .db("books")
      .collection<Book>("saved")
      .findOne({ id: code });

    if (book) return book;

    throw new Error("Not found");
  }

  async save(book: Book): Promise<void> {
    await this.client.db("books").collection("saved").insertOne(book);
  }

  async saveMany(books: Book[]): Promise<void> {
    await this.client.db("books").collection("saved").insertMany(books);
  }
}
