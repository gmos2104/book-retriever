import { Book } from "../../domain/entities/book.entity";
import { EventAction } from "../../domain/enums/event-action.enum";
import { ReadableBookRepository } from "../../domain/interfaces/book.repository";
import { BookDTO } from "../dtos/book.dto";
import { DataNotFoundError } from "../errors/data-not-found.error";
import { ServiceUnavailableError } from "../errors/service-unavailable.error";

export class FakeStoreBookRepository implements ReadableBookRepository {
  private BASE_URL = "https://fake-book-store-api.onrender.com";

  async all(): Promise<Book[]> {
    const response = await fetch(new URL("/api/books", this.BASE_URL), {
      method: "GET",
    });

    if (!response.ok) {
      if (response.status === 400) {
        throw new DataNotFoundError(EventAction.LIST);
      } else {
        throw new ServiceUnavailableError();
      }
    }

    const books = (await response.json()) as BookDTO[];

    return books.map(BookDTO.toDomain);
  }

  async find(code: string): Promise<Book> {
    const response = await fetch(new URL(`/api/books/${code}`, this.BASE_URL), {
      method: "GET",
    });

    if (!response.ok) {
      if (response.status === 400) {
        throw new DataNotFoundError(EventAction.GET, code);
      } else {
        throw new ServiceUnavailableError();
      }
    }

    const book = (await response.json()) as BookDTO;

    return BookDTO.toDomain(book);
  }
}
