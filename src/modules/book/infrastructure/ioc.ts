import { ProcessBookEventUseCase } from "../application/use-case/process-book-event.use-case";
import { RetrieveSavedBooksUseCase } from "../application/use-case/retrieve-saved-books.use-case";
import { FakeStoreBookRepository } from "./repositories/fake-store-book.repository";
import { MongoBookRepository } from "./repositories/mongo-book.repository";

export const processBookEventUseCase = new ProcessBookEventUseCase(
  new FakeStoreBookRepository(),
  new MongoBookRepository(),
);

export const retrieveSavedBooksUseCase = new RetrieveSavedBooksUseCase(
  new MongoBookRepository(),
);
